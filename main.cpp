#include <QCoreApplication>
#include <iostream>
#include <omp.h>
#include <QtConcurrentMap>
#include <QTime>
#include <functional>

template<class F, class D>
void benchAll(std::string name,F task, D data){
	std::cout << std::endl << "Test: "<< name << std::endl;
	double refTime;

	auto bench=[&](std::string s,std::function<void()> f){
		auto t0=QTime::currentTime().msecsSinceStartOfDay();
		f();
		auto deltaT=QTime::currentTime().msecsSinceStartOfDay()-t0;
		std::cout << s <<" msec: "<<deltaT  << " ==> "<< refTime/deltaT <<" times faster than ref."<< std::endl;
	};

	auto stdAutoRef = [&](){
		for (auto& c : data) {
			task(c);
		}
	};

	auto t0=QTime::currentTime().msecsSinceStartOfDay();
	stdAutoRef();
	refTime=QTime::currentTime().msecsSinceStartOfDay()-t0;

	//////////////////////////////
	//////////////////////////////
	//////////////////////////////

	bench("std auto&",stdAutoRef);

	bench("std for(i)",[&](){
		for (auto& c : data) {
			task(c);
		}
	});

	bench("Qt blockingMap",[&](){
		QtConcurrent::blockingMap(data,task);
	});

	bench("omp par for(i)",[&](){
#pragma omp parallel for
		for (int i = 0; i < data.size(); ++i) {
			task(data[i]);
		}
	});
}

int main(int argc, char *argv[]){
	std::string data="pajdpoiazjdpiajda";
	benchAll("pow",[](char& c){
		for (int i = 0; i < 100000000; ++i) {
			c=c*c;
		}
	},
	data
	);

	benchAll("++",[](char& c){
		for (int i = 0; i < 100000000; ++i) {
			c++;
		}
	},
	data
	);

	benchAll("big math",[](char& c){
		for (int i = 0; i < 20000000; ++i) {
			c=c*c*c*c*c+c%c;
		}
	},
	data
	);
}
